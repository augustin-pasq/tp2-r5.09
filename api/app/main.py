from dotenv import load_dotenv
from fastapi import FastAPI
import mysql.connector
import os

app = FastAPI()

# Load environment variables
load_dotenv()

MYSQL_HOST = os.getenv("MYSQL_HOST")
MYSQL_PORT = os.getenv("MYSQL_PORT")
MYSQL_USER = os.getenv("MYSQL_USER")
MYSQL_PASSWORD = os.getenv("MYSQL_PASSWORD")
MYSQL_DATABASE = os.getenv("MYSQL_DATABASE")

def connect():
    return mysql.connector.connect(
        host=MYSQL_HOST,
        port=MYSQL_PORT,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        database=MYSQL_DATABASE
    )

@app.post("/addCustomer")
async def add_customer(lastname: str, firstname: str, email: str, command_number: int):
    database = connect()
    
    query = "INSERT INTO Customer (lastname, firstname, email, command_number) VALUES (%s, %s, %s, %s);"
    values = (lastname, firstname, email, command_number)
    
    cursor = database.cursor()
    cursor.execute(query, values)
    database.commit()
    lastrowid = cursor.lastrowid
    database.close()
    
    return lastrowid

@app.put("/editCustomer/{id}")
async def edit_customer(id: str, lastname: str, firstname: str, email: str, command_number: int):
    database = connect()
    
    query = "UPDATE Customer SET lastname = '" + lastname + "', firstname = '" + firstname + "', email = '" + email + "', command_number = " + str(command_number) + " WHERE id = " + id + ";"

    cursor = database.cursor()
    cursor.execute(query)
    database.commit()

    lastrowid = cursor.lastrowid
    database.close()
    
    return lastrowid

@app.delete("/deleteCustomer/{id}")
async def delete_customer(id: str):
    database = connect()
    
    query = "DELETE FROM Customer WHERE id = " + id + ";"

    cursor = database.cursor()
    cursor.execute(query)
    database.commit()

    lastrowid = cursor.lastrowid
    database.close()
    
    return lastrowid

@app.get("/getCustomer/{id}")
async def get_customer(id: str):
    database = connect()
    
    query = "SELECT * FROM Customer WHERE id = " + id + ";"
    
    cursor = database.cursor()
    cursor.execute(query)
    result = cursor.fetchone()
    database.close()
    
    return result

@app.get("/getCustomers")
async def get_customers():
    database = connect()
    
    query = "SELECT * FROM Customer;"
    
    cursor = database.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    database.close()
    
    return result